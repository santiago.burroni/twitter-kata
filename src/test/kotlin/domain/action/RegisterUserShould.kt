package domain.action

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import domain.model.user.User
import domain.model.user.UserAlreadyRegisteredException
import domain.model.user.UserService
import org.junit.Test

class RegisterUserShould {

    private lateinit var action: RegisterUser
    private val service: UserService = mock()

    private lateinit var exception: RuntimeException

    @Test
    fun `Register user successfully when nickName unique`() {
        givenAnAction()
        whenRegisterUser()
        thenVerifyUserIsRegistered()
    }

    @Test
    fun `Register user unsuccessfully when nickName is taken`() {
        givenAnAction()
        givenAServiceThatThrowsAnException()
        whenRegisterUserUnsuccessfully()
        thenThrowUserAlreadyRegisteredException()
    }

    private fun givenAnAction() {
        action = RegisterUser(service)
    }

    private fun givenAServiceThatThrowsAnException() {
        whenever(service.register(user)).thenThrow(UserAlreadyRegisteredException())
    }

    private fun whenRegisterUser() {
        action(user.nickName, user.realName)
    }

    private fun whenRegisterUserUnsuccessfully() {
        try {
            action(user.nickName, user.realName)
        } catch (e: RuntimeException) {
            exception = e
        }
    }

    private fun thenVerifyUserIsRegistered() {
        verify(service).register(user)
    }

    private fun thenThrowUserAlreadyRegisteredException() {
        assert(exception is UserAlreadyRegisteredException)
    }

    companion object {
        val user = User("@facu", "Facu")
    }
}