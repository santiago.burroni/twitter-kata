package domain.action

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import domain.model.user.User
import domain.model.user.UserService
import org.junit.Test

class UpdateUserRealNameShould {

    private lateinit var action: UpdateUserRealName
    private val service: UserService = mock()

    @Test
    fun `Update user realName`() {
        givenAnAction()
        whenUpdateUserRealName()
        thenVerifyUserIsUpdated()
    }

    private fun givenAnAction() {
        action = UpdateUserRealName(service)
    }

    private fun whenUpdateUserRealName() {
        action(updatedUser.nickName, updatedUser.realName)
    }

    private fun thenVerifyUserIsUpdated() {
        verify(service).updateRealName(updatedUser)
    }

    companion object {
        val updatedUser = User("@facu", "Facundo")
    }
}