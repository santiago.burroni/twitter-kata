package domain.action

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import domain.model.user.User
import domain.model.user.FollowIntent
import domain.model.user.UserService
import org.junit.Test

class FollowUserShould {

    private lateinit var action: FollowUser
    private val service: UserService = mock()

    @Test
    fun `Follow the given user`() {
        givenAnAction()
        whenFollowUser()
        thenVerifyUserIsFollowed()
    }

    private fun givenAnAction() {
        action = FollowUser(service)
    }

    private fun whenFollowUser() {
        action(user.nickName, followedUser.nickName)
    }

    private fun thenVerifyUserIsFollowed() {
        verify(service).follow(FollowIntent(user.nickName, followedUser.nickName))
    }

    companion object {
        val user = User("@facu", "Facundo")
        val followedUser = User("@dvk", "Santiago")
    }
}