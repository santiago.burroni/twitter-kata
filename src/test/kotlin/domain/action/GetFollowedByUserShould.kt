package domain.action

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import domain.model.user.User
import domain.model.user.UserService
import org.junit.Test

class GetFollowedByUserShould {

    private lateinit var action: GetFollowedByUser
    private val service: UserService = mock()
    private lateinit var result: List<User>

    @Test
    fun `Return users that are followed by the given user`() {
        givenAnAction()
        givenAServiceThatReturnsAnUserList()
        whenGetUsersFollowedByUser()
        thenVerifyUsersFollowedByUserWereReturned()
    }

    private fun givenAnAction() {
        action = GetFollowedByUser(service)
    }

    private fun givenAServiceThatReturnsAnUserList() {
        whenever(service.getUsersFollowedBy(user.nickName)).thenAnswer { usersFollowed }
    }

    private fun whenGetUsersFollowedByUser() {
        result = action(user.nickName)
    }

    private fun thenVerifyUsersFollowedByUserWereReturned() {
        assert(result == usersFollowed)
    }

    companion object {
        val user = User("@facu", "Facundo")
        val usersFollowed = listOf(
            User("@dvk", "Santiago"),
            User("@flx", "Felipe")
        )
    }
}