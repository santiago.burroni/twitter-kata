package domain.model.user

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import org.junit.Test
import java.lang.RuntimeException

class UserServiceShould {

    private val repository: UserRepository = mock()

    private lateinit var service: UserService

    private lateinit var exception: RuntimeException

    lateinit var usersFollowedByUserResult: List<User>

    @Test
    fun `Add new user to the repository when nickName is not taken`() {
        givenAService()
        whenRegisterUser()
        thenVerifyUserIsAdded()
    }

    @Test
    fun `Throw Exception when adding an existing user`() {
        givenAService()
        givenARepositoryThatThrowsAnException()
        whenRegisterUserUnsuccessfully()
        thenThrowUserAlreadyRegisteredException()
    }

    @Test
    fun `Update user realName when the user exists`() {
        givenAService()
        givenARepositoryThatReturnsAnUser()
        whenUpdateRealName()
        thenVerifyUserIsUpdated()
    }

    @Test
    fun `Update user to add a followed user`() {
        givenAService()
        givenARepositoryThatReturnsUsers()
        whenFollowUser()
        thenVerifyUserWasFollowed()
    }

    @Test
    fun `Return list of users being followed by the given user`() {
        givenAService()
        givenARepositoryThatReturnsAUserWhoFollows()
        whenGetUsersFollowedBy()
        thenVerifyUsersFollowedByUserWereReturned()
    }

    private fun givenAService() {
        service = UserServiceSimple(repository)
    }

    private fun givenARepositoryThatReturnsAnUser() {
        whenever(repository.get(updatedUser.nickName)).thenAnswer { user }
    }

    private fun givenARepositoryThatThrowsAnException() {
        whenever(repository.add(user)).thenThrow(UserAlreadyRegisteredException())
    }

    private fun givenARepositoryThatReturnsUsers() {
        whenever(repository.get(user.nickName)).thenAnswer { user }
        whenever(repository.get(followedUser.nickName)).thenAnswer { followedUser }
    }

    private fun givenARepositoryThatReturnsAUserWhoFollows() {
        whenever(repository.get(user.nickName)).thenAnswer { userWhoFollows }
        whenever(repository.get(followedUser.nickName)).thenAnswer { followedUser }
    }

    private fun whenRegisterUser() {
        service.register(user)
    }

    private fun whenRegisterUserUnsuccessfully() {
        try {
            service.register(user)
        } catch (e: RuntimeException) {
            exception = e
        }
    }

    private fun whenUpdateRealName() {
        service.updateRealName(updatedUser)
    }

    private fun whenFollowUser() {
        service.follow(FollowIntent(user.nickName, followedUser.nickName))
    }

    private fun whenGetUsersFollowedBy() {
        usersFollowedByUserResult = service.getUsersFollowedBy(user.nickName)
    }

    private fun thenVerifyUserIsAdded() {
        verify(repository).add(user)
    }

    private fun thenThrowUserAlreadyRegisteredException() {
        assert(exception is UserAlreadyRegisteredException)
    }

    private fun thenVerifyUserIsUpdated() {
        verify(repository).add(updatedUser)
    }

    private fun thenVerifyUserWasFollowed() {
        verify(repository).add(userWhoFollows)
    }

    private fun thenVerifyUsersFollowedByUserWereReturned() {
        assert(nickNamesOfReturnedUsers() == userWhoFollows.followingNickNameList)
    }

    private fun nickNamesOfReturnedUsers() = usersFollowedByUserResult.map { it.nickName }

    companion object {
        private val user = User("@facu", "Facu")
        private val updatedUser = User("@facu", "Facundo")

        private val followedUser = User("@flx", "Felipe")
        private val userWhoFollows = user.copy(followingNickNameList = listOf(followedUser.nickName))
    }
}