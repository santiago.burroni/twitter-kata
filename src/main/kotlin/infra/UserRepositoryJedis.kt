package infra

import com.google.gson.Gson
import domain.model.user.User
import domain.model.user.UserRepository
import redis.clients.jedis.Jedis


class UserRepositoryJedis(private val jedis: Jedis, private val gson: Gson) : UserRepository {

    override fun add(user: User) {
        jedis.set(user.nickName, gson.toJson(user))
    }

    override fun get(nickName: String): User? = jedis.get(nickName)?.let { gson.fromJson(it, User::class.java) }

    override fun remove(user: User) {
        jedis.del(user.nickName)
    }
}