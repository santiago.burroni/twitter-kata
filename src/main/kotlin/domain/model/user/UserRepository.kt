package domain.model.user

interface UserRepository {
    fun add(user: User)
    fun get(nickName: String): User?
    fun remove(user: User)
}