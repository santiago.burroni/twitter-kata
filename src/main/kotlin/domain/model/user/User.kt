package domain.model.user

data class User(
    val nickName: String,
    val realName: String,
    val followingNickNameList: List<String> = listOf()
)