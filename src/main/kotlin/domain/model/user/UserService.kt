package domain.model.user

interface UserService {
    fun register(user: User)
    fun updateRealName(newUser: User)
    fun follow(intent: FollowIntent)
    fun getUsersFollowedBy(nickName: String): List<User>
}