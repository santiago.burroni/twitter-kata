package domain.model.user

class UserServiceSimple(private val repository: UserRepository) : UserService {

    override fun register(user: User) {
        repository.add(user)
    }

    override fun updateRealName(newUser: User) {
        repository.apply {
            get(newUser.nickName)?.let { user ->
                updateUser(user, newUser)
            }
        }
    }

    override fun follow(intent: FollowIntent) {
        getUser(intent)?.let { user ->
            if (isStranger(user, intent.toFollow)) {
                getUserToFollow(intent)?.let { followedUser ->
                    addToFollowingList(user, followedUser)
                }
            }
        }
    }

    private fun addToFollowingList(user: User, followedUser: User) {
        updateUser(user, copyUserFollowingAnother(user, followedUser))
    }

    private fun getUserToFollow(intent: FollowIntent) =
        repository.get(intent.toFollow)

    private fun getUser(intent: FollowIntent) =
        repository.get(intent.nickName)

    override fun getUsersFollowedBy(nickName: String) = repository.get(nickName)?.let { user ->
        user.followingNickNameList.mapNotNull { repository.get(it) }
    } ?: listOf()

    private fun copyUserFollowingAnother(user: User, followedUser: User) =
        user.copy(followingNickNameList = user.followingNickNameList.plus(followedUser.nickName))

    private fun updateUser(user: User, newUser: User) {
        repository.remove(user)
        repository.add(newUser)
    }

    private fun isStranger(
        user: User,
        followUserNickName: String
    ) = user.followingNickNameList.none { it == followUserNickName }
}