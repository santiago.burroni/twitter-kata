package domain.action

import domain.model.user.User
import domain.model.user.UserService

class UpdateUserRealName(private val service: UserService) {

    operator fun invoke(nickName: String, newRealName: String) {
        service.updateRealName(User(nickName, newRealName))
    }
}