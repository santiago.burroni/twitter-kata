package domain.action

import domain.model.user.User
import domain.model.user.UserService

class RegisterUser(private val service: UserService) {

    operator fun invoke(nickName: String, realName: String) {
        service.register(User(nickName, realName))
    }
}