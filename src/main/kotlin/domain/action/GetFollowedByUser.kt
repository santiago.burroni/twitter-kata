package domain.action

import domain.model.user.UserService

class GetFollowedByUser(private val service: UserService) {

    operator fun invoke(nickName: String) = service.getUsersFollowedBy(nickName)
}