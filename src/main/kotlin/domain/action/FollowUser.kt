package domain.action

import domain.model.user.FollowIntent
import domain.model.user.UserService

class FollowUser(private val service: UserService) {

    operator fun invoke(nickName: String, toFollow: String) {
        service.follow(FollowIntent(nickName, toFollow))
    }
}