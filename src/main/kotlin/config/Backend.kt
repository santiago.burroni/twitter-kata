package config

import com.google.gson.Gson
import domain.action.FollowUser
import domain.action.GetFollowedByUser
import domain.action.RegisterUser
import domain.action.UpdateUserRealName
import domain.model.user.UserService
import io.javalin.Javalin

class Backend(private val javalin: Javalin, private val userService: UserService, private val gson: Gson) {

    fun setupEndpoints() {
        addRegisterUserEndpoint()
        addUpdateUserRealNameEndpoint()
        addFollowUserEndpoint()
        addGetFollowedByUserEndpoint()
    }

    private fun addRegisterUserEndpoint() {
        javalin.get(REGISTER_USER_PATH) { context ->
            val nickName = context.queryParam(NICK_NAME_QUERY_PARAMETER)
            val realName = context.queryParam(REAL_NAME_QUERY_PARAMETER)
            if (nickName != null && realName != null) {
                RegisterUser(userService)(nickName, realName)
                context.result(SUCCESS_CODE)
            } else context.result(FAILURE_CODE)
        }
    }

    private fun addUpdateUserRealNameEndpoint() {
        javalin.get(UPDATE_USER_REAL_NAME_PATH) { context ->
            val nickName = context.queryParam(NICK_NAME_QUERY_PARAMETER)
            val realName = context.queryParam(REAL_NAME_QUERY_PARAMETER)
            if (nickName != null && realName != null) {
                UpdateUserRealName(userService)(nickName, realName)
                context.result(SUCCESS_CODE)
            } else context.result(FAILURE_CODE)
        }
    }

    private fun addFollowUserEndpoint() {
        javalin.get(FOLLOW_USER_PATH) { context ->
            val nickName = context.queryParam(NICK_NAME_QUERY_PARAMETER)
            val toFollow = context.queryParam(TO_FOLLOW_QUERY_PARAMETER)
            if (nickName != null && toFollow != null) {
                FollowUser(userService)(nickName, toFollow)
                context.result(SUCCESS_CODE)
            } else context.result(FAILURE_CODE)
        }
    }

    private fun addGetFollowedByUserEndpoint() {
        javalin.get(GET_FOLLOWED_BY_USER_PATH) { context ->
            context.queryParam(NICK_NAME_QUERY_PARAMETER)?.let { nickName ->
                context.result(gson.toJson(GetFollowedByUser(userService)(nickName)))
            } ?: context.result(FAILURE_CODE)
        }
    }

    private companion object {
        const val REGISTER_USER_PATH = "/registerUser/"
        const val UPDATE_USER_REAL_NAME_PATH = "/updateUserRealName/"
        const val FOLLOW_USER_PATH = "/followUser/"
        const val GET_FOLLOWED_BY_USER_PATH = "/getFollowedByUser/"

        const val NICK_NAME_QUERY_PARAMETER = "nickName"
        const val REAL_NAME_QUERY_PARAMETER = "realName"
        const val TO_FOLLOW_QUERY_PARAMETER = "toFollow"

        const val SUCCESS_CODE = "200"
        const val FAILURE_CODE = "500"
    }
}