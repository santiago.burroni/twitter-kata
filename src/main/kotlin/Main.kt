import com.google.gson.Gson
import config.Backend
import domain.model.user.UserServiceSimple
import infra.UserRepositoryJedis
import io.javalin.Javalin
import redis.clients.jedis.Jedis

fun main() {
    val gson = Gson()
    Backend(
        Javalin.create().start(HOST, PORT_JAVALIN),
        UserServiceSimple(UserRepositoryJedis(Jedis(HOST, PORT_JEDIS), gson)),
        gson
    ).setupEndpoints()
}

const val HOST = "localhost"

const val PORT_JAVALIN = 7000
const val PORT_JEDIS = 6379